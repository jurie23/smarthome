import 'package:flutter/widgets.dart';
import 'package:smarthome/main.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:flutter/material.dart';
import '../style.dart';

class GaugeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("Sensor Data"),
      ),
      body: Stack(children: <Widget>[
        Background('assets/images/7vMmx.jpg'),
        Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 10),
              child: SfRadialGauge(axes: <RadialAxis>[
                RadialAxis(minimum: 300, maximum: 1400, ranges: <GaugeRange>[
                  GaugeRange(
                      startValue: 300,
                      endValue: 700,
                      color: Colors.green,
                      startWidth: 10,
                      endWidth: 10),
                  GaugeRange(
                      startValue: 700,
                      endValue: 1000,
                      color: Colors.orange,
                      startWidth: 10,
                      endWidth: 10),
                  GaugeRange(
                      startValue: 1000,
                      endValue: 1400,
                      color: Colors.red,
                      startWidth: 10,
                      endWidth: 10)
                ], pointers: <GaugePointer>[
                  NeedlePointer(value: 90)
                ], annotations: <GaugeAnnotation>[
                  GaugeAnnotation(
                      widget: Container(
                          child: Text('700 ppm',
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold))),
                      angle: 90,
                      positionFactor: 0.5)
                ])
              ]),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  child: Text(
                    "Temp: 21°",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    "Laut: 55 dB",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ],
        )
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Homepage()),
          );
        },
        child: Icon(Icons.arrow_forward_ios),
        backgroundColor: Colors.lightBlue,
      ),
    ));
  }
}
