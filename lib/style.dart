import 'dart:ui';

import 'package:smarthome/Charts/gauge.dart';

import 'main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

const LargeTextSize = 26.0;
const MediumTextSize = 20.0;
const BodyTextSize = 16.0;

const String FontNameDefault = 'Montserrat';

const AppBarTextStyle = TextStyle(
  fontFamily: FontNameDefault,
  fontWeight: FontWeight.w300,
  fontSize: MediumTextSize,
  color: Colors.white,
);

const TitleTextStyle = TextStyle(
  fontFamily: FontNameDefault,
  fontWeight: FontWeight.w300,
  fontSize: LargeTextSize,
  color: Colors.black,
);

const Body1TextStyle = TextStyle(
  fontFamily: FontNameDefault,
  fontWeight: FontWeight.w300,
  fontSize: BodyTextSize,
  color: Colors.black,
);

class TextBanner extends StatelessWidget {
  final String _text;

  TextBanner(this._text);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 270,
      child: TextButton(
        child: Text(_text, style: Theme.of(context).textTheme.headline6),
        style: TextButton.styleFrom(
          primary: Colors.white,
          backgroundColor: Colors.cyan[600],
          shadowColor: Colors.green,
          elevation: 10,
        ),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => GaugeScreen()),
          );
        },
      ),
    );
  }
}

class ImageBanner extends StatelessWidget {
  final String _assetPath;

  ImageBanner(this._assetPath);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(
        height: 70.0,
      ),
      decoration: BoxDecoration(color: Colors.grey),
      child: Image.asset(
        _assetPath,
        fit: BoxFit.cover,
      ),
    );
  }
}

class Background extends StatelessWidget {
  final String _assetPath;

  Background(this._assetPath);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(height: 800),
      decoration: BoxDecoration(color: Colors.grey),
      child: Image.asset(
        _assetPath,
        fit: BoxFit.cover,
      ),
    );
  }
}
