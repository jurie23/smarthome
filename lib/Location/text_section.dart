import 'package:flutter/material.dart';

class Texting extends StatelessWidget {
  final String _title;
  final String _body;
  static const double _hPad = 16.0;
  //_ at beginning stands for private member
  // can't be accesed outside this widget

  // Constructor
  Texting(this._title, this._body);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: const EdgeInsets.fromLTRB(_hPad, 32.0, _hPad, 4.0),
            child: Text(_title, style: Theme.of(context).textTheme.headline6),
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(_hPad, 32.0, _hPad, 4.0),
            child: Text(_body, style: Theme.of(context).textTheme.bodyText1),
          )
        ]);
  }
}
