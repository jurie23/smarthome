import 'package:flutter/material.dart';
import 'text_section.dart';
import '../style.dart';

class LocationDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final locations = Location.fetchAll();
    final location = locations.first;

    return Scaffold(
        appBar: AppBar(title: Text(location.name)),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ImageBanner(location.imgPath),
          ]..addAll(textSections(
              location)), // add all iterable objects to end of list (children is already a list )
        ));
  }

  List<Widget> textSections(Location location) {
    return location.facts
        .map((fact) => Texting(fact.title, fact.text))
        .toList();
  }
}

class Location {
  String name;
  String imgPath;
  final List<LocationFact> facts;

  Location(this.name, this.imgPath, this.facts);

  static List<Location> fetchAll() {
    return [
      Location('name', 'assets/images/Holzpenis4.png', [
        LocationFact('Summary', 'This is a fact'),
        LocationFact('How to get there', 'follow your instinct')
      ])
    ];
  }
}

class LocationFact {
  final String title;
  final String text;

  LocationFact(this.title, this.text);
}
