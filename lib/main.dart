import 'package:flutter/material.dart';
import 'Location/location_detail.dart';
import 'style.dart';
import 'Charts/gauge.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Homepage(),
        theme: ThemeData(
            appBarTheme:
                AppBarTheme(textTheme: TextTheme(headline6: AppBarTextStyle)),
            textTheme: TextTheme(
              headline6: TitleTextStyle,
              bodyText1: Body1TextStyle,
            )));
  }
}

class Homepage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Home')),
        drawer: Drawer(
          child: ListView(children: [
            UserAccountsDrawerHeader(
              accountName: Text('Julius'),
              accountEmail: Text('Juli.Riegger@gmx.de'),
              currentAccountPicture: CircleAvatar(
                  backgroundImage: AssetImage('assets/images/Holzpenis4.png')),
            ),
            new ListTile(
              title: Text('Location'),
              onTap: () {
                Navigator.of(context)
                    .pop(); // pops side menu after going to extra content of menue
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => LocationDetail()));
              },
            ),
            new ListTile(
              title: Text('CO2 Gauge'),
              onTap: () {
                Navigator.of(context)
                    .pop(); // pops side menu after going to extra content of menue
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => GaugeScreen()));
              },
            )
          ]),
        ),
        body: Stack(
          children: <Widget>[
            Background('assets/images/7vMmx.jpg'),
            Center(
                child: Column(
              children: <Widget>[
                SizedBox(height: 100),
                FloatingActionButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Homepage()),
                    );
                  },
                  child: Icon(Icons.bluetooth),
                  backgroundColor: Colors.lightBlue,
                ),
                SizedBox(height: 100),
                TextBanner("Check CO2"),
                SizedBox(height: 20),
                TextBanner("See something else"),
              ],
            ))
          ],
        ));

    // return Stack(children: <Widget>[
    //   Background('assets/images/7vMmx.jpg'),
    //   Scaffold(
    //       appBar: AppBar(title: Text('Home')),
    //       drawer: Drawer(
    //         child: ListView(children: [
    //           UserAccountsDrawerHeader(
    //             accountName: Text('Julius'),
    //             accountEmail: Text('Juli.Riegger@gmx.de'),
    //             currentAccountPicture: CircleAvatar(
    //                 backgroundImage:
    //                     AssetImage('assets/images/Holzpenis4.png')),
    //           ),
    //           new ListTile(
    //             title: Text('Location'),
    //             onTap: () {
    //               Navigator.of(context)
    //                   .pop(); // pops side menu after going to extra content of menue
    //               Navigator.push(
    //                   context,
    //                   MaterialPageRoute(
    //                       builder: (BuildContext context) => LocationDetail()));
    //             },
    //           ),
    //           new ListTile(
    //             title: Text('CO2 Gauge'),
    //             onTap: () {
    //               Navigator.of(context)
    //                   .pop(); // pops side menu after going to extra content of menue
    //               Navigator.push(
    //                   context,
    //                   MaterialPageRoute(
    //                       builder: (BuildContext context) => GaugeScreen()));
    //             },
    //           )
    //         ]),
    //       ),
    //       body: Center(
    //         child: Column(
    //           children: <Widget>[
    //             SizedBox(height: 100),
    //             TextBanner("Check CO2"),
    //             SizedBox(height: 20),
    //             TextBanner("See something else"),
    //           ],
    //         ),
    //       ))
    // ]);
  }
}
